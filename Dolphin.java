public class Dolphin extends Animal implements getLegNum{

    private int legNum;

    public Dolphin(String name,int legNum)
    {
        super(name);
        this.legNum=legNum;
    }

    public String shout()
    {
        return "海豚音~~~";
    }

    public int getLegNum()
    {
        return legNum;
    }

}
