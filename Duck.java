public class Duck extends Animal implements getLegNum{

    private int legNum;

    public Duck(String name,int legNum)
    {
        super(name);
        this.legNum=legNum;
    }

    public String shout()
    {
        return "嘎嘎嘎~~~";
    }

    public int getLegNum()
    {
        return legNum;
    }

}
