public class Cat extends Animal implements getLegNum{

    private int legNum;

    public Cat(String name,int legNum)
    {
        super(name);
        this.legNum=legNum;
    }

    public String shout()
    {
        return "喵喵喵~~~";
    }

    public int getLegNum()
    {
        return legNum;
    }

}
