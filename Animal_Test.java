import java.util.Scanner;

public class Animal_Test {

    static Scanner input=new Scanner(System.in);

    static public void date1s()
    {
        long ch=System.currentTimeMillis();
        long c=0;
        do{
            c=System.currentTimeMillis();
        }while(((int)((c-ch)/1000))<1);
    }
    static Animal animal[]=new Animal[3];
    static{
    animal[0]=new Cat("加菲猫",4);
    animal[1]=new Duck("唐小鸭",2);
    animal[2]=new Dolphin("海豚奇奇",0);
    }

    public static void showPrint()
    {
        System.out.println("动物名字\t\t腿的条数\t\t叫声");
        for (Animal animal1 : animal) {
            if (animal1 instanceof Cat) {
                Cat cat = (Cat) animal1;
                System.out.println(cat.getName() + "\t\t" + cat.getLegNum() + "\t\t" + cat.shout());
            } else if (animal1 instanceof Duck) {
                Duck duck = (Duck) animal1;
                System.out.println(duck.getName() + "\t\t" + duck.getLegNum() + "\t\t" + duck.shout());
            } else if (animal1 instanceof Dolphin) {
                Dolphin dolphin = (Dolphin) animal1;
                System.out.println(dolphin.getName() + "\t\t" + dolphin.getLegNum() + "\t\t" + dolphin.shout());
            }
        }
    }

    public static void revision ()
    {
        for(Animal animal1 : animal)
        {
            if(animal1 instanceof Cat)
            {
                Cat cat = (Cat) animal1;
                System.out.print("请输入猫的名称：");
                String name=input.next();
                System.out.print("请输入猫腿的条数：");
                int oldLeg=cat.getLegNum();
                int newLeg=input.nextInt();
                try {
                    if (newLeg != oldLeg) {
                        throw new LegNumException("健康猫的腿只能是4条");
                    }
                    cat.setName(name);
                }catch (LegNumException w)
                {
                    w.printStackTrace();
                    date1s();
                    break;
                }
            }else if(animal1 instanceof Duck)
            {
                Duck duck = (Duck) animal1;
                System.out.print("请输入鸭子的名称：");
                String name=input.next();
                System.out.print("请输入鸭子腿的条数：");
                int oldLeg=duck.getLegNum();
                int newLeg=input.nextInt();
                try {
                    if (newLeg != oldLeg) {
                        throw new LegNumException("健康鸭子的腿只能是2条");
                    }
                    duck.setName(name);
                }catch (LegNumException w)
                {
                    w.printStackTrace();
                    date1s();
                    break;
                }
            }
            else if(animal1 instanceof Dolphin)
            {
                Dolphin dolphin = (Dolphin) animal1;
                System.out.print("请输入海豚的名称：");
                String name=input.next();
                System.out.print("请输入海豚腿的条数：");
                int oldLeg=dolphin.getLegNum();
                int newLeg=input.nextInt();
                try{
                    if(newLeg!=oldLeg)
                    {
                        throw new LegNumException("你是不是傻，海豚有腿吗！");
                    }
                    dolphin.setName(name);
                }catch (LegNumException w)
                {
                    w.printStackTrace();
                    date1s();
                    break;
                }
            }

        }
    }
    public static void main(String[] args) {


        int n=0;
        do {
            n=-1;
            showPrint();
            System.out.println("是否要继续修改数据，按0进行修改，其他任意数字键退出");
            n=input.nextInt();
            if(n==0)
            {
                revision();
            }

        }while (n==0);

    }
}
